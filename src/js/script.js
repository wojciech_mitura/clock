document.addEventListener('DOMContentLoaded', (event) => {
    const secondHand = document.getElementById('second');

    setTimeout(function() {
        secondHand.classList.add('clock__hand--transition');
    }, 1000);

    function time() {
        const secondHand = document.getElementById('second');
        const minuteHand = document.getElementById('minute');
        const hourHand = document.getElementById('hour');
        const date = new Date();
        const sec = date.getSeconds();
        const second = sec / 60;
        const minute = (date.getMinutes() + second) / 60;
        const hour = (date.getHours() + minute) / 12;

        secondHand.style.transform = 'rotate(' + (second * 360) + 'deg)';
        minuteHand.style.transform = 'rotate(' + (minute * 360) + 'deg)';
        hourHand.style.transform = 'rotate(' + (hour * 360) + 'deg)';

        if (sec === 0) {
            secondHand.classList.remove('lock__hand--transition');
            secondHand.classList.add('clock__hand-second--animation');
        }
    }

    setTimeout(
        function() {
            time();
            setInterval(time, 1000);
        }, 10
    );

    function date() {
        const weekday = ['Sunday', 'Monday', 'Tuesday', 'Wendnesday', 'Thursday', 'Friday', 'Saturday'];
        const date = new Date();
        const dateDay = date.getDate();
        const dateMonth = date.getMonth() + 1;
        const dateYear = date.getFullYear();

        const dataDisplay = document.querySelector('.clock__date');
        const dayDisplay = document.querySelector('.clock__day');

        dayDisplay.innerText = weekday[date.getDay()];
        dataDisplay.innerText = dateDay + '/' + dateMonth + '/' + dateYear;
    }

    date();
});